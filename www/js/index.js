var openWeatherCall = function(url){
    $.ajax({
        url: url,
        type: 'GET',
        data:{
            format: 'json'
        },
        dataType: 'jsonp',
        error: function() {
            alert('error');
        },
        success: function(data) {
            $('#ville').text(data.name);
            $('#temperature').text(data.main.temp + "°C");
            $('#vent').text(data.wind.speed + "m/s");
            $('#humidite').text(data.main.humidity + "%");
            var image = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";
            $("#image").attr("src",image);
        }
    });
}

$("#search").submit(function(event) {
    event.preventDefault();
    var zipCode = $("#zip_code").val();
    var url = 'http://api.openweathermap.org/data/2.5/weather?zip=' + zipCode + ',fr&APPID=7b70424e0e67656c4a24504bb4e5abff&units=metric';
    openWeatherCall(url);

});


$("#geoloc").click(function(event) {

    var onSuccess = function(position) {
        var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&appid=7b70424e0e67656c4a24504bb4e5abff&units=metric";
        openWeatherCall(url);
    };

// onError Callback receives a PositionError object
//
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
});


